import './App.css';
import Navbar from './Components/Navbar';
import pic from './4565.jpg';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
function App() {
  return (
    <Router>
      <div className="App" style={{ backgroundImage: `url(${pic})`}} >
        <Navbar />
        <Router path="/" exact component={App}/>
      
      </div>
    </Router> 
  );
}

export default App;

import React from 'react'
import {BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom'

const Navbar = () => {
    return (
        <div className = "nav-bar"> 
            <h1>Goom</h1>
            <div className="nav-item-container" >
                <ul className="nav-items">
                    <li className="item"><Link to="/" style={{textDecoration: 'none'}}>Home</Link></li>
                    <li className="item">About</li>
                    <li className="item">Sign In</li>
                    <li className="item">Sign Up</li>   
                </ul>
            </div>  
        </div>
    )
}

export default Navbar
